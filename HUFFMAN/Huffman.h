



#include<iostream>
#include<fstream>
#include<string>
#include<iomanip>
#include<cctype>


using namespace std;

ofstream Table;
string table[256];
int sayac;

template<class T>
class Queue
{
public:

	Queue(int d = 2);
	~Queue(void);
	void enq(T*);
	T* deq(void);
	T* front(void);
	bool empty(void) const;
	bool full(void) const;

private:

	int back;
	T* *arr;
	int size;
	static const int SIZE = 10;
	int D;
	Queue(const Queue &);
	const Queue & operator=(const Queue &);

	void reheapup(int, int);
	void reheapdown(int, int);
	void swap(T* &, T* &);

};


template<class T>
Queue<T>::Queue(int d)
{
	if (d<2) d = 2;
	D = d;
	back = 0;
	size = SIZE;
	arr = new T*[size];
}

template<class T>
bool Queue<T>::empty(void) const
{
	return (back <= 0);
}

template<class T>
bool Queue<T>::full(void) const
{
	return (back >= size);
}

template<class T>
T* Queue<T>::deq(void)
{
	if (empty())
	{
		cerr << "deq error! exiting..." << endl;
		exit(1);
	}

	T* rval = arr[0];
	arr[0] = arr[back - 1];
	--back;
	reheapdown(0, back - 1);
	return rval;
}
template<class T>
T* Queue<T>::front(void)
{
	if (empty())
	{
		cerr << "deq error! exiting..." << endl;
		exit(1);
	}

	return arr[0];
}

template<class T>
void Queue<T>::enq(T* foo)
{
	if (full()) //if the array is full then make it larger
	{
		int nsize = size + SIZE; //the size of the new array
		T* *narr = new T*[nsize]; //new array
		for (int i = 0; i<size; ++i) //copy old array to the new one
			narr[i] = arr[i];
		delete[] arr; //delete reserved old array mem
		arr = narr; //pointer update
		size = nsize; //size update
	}

	//the new element added to the back of the queue
	//and the reheapup called to fix the order back
	arr[back++] = foo; //arr[back]=foo;++back;
	reheapup(0, back - 1);
}


template<class T>
void Queue<T>::reheapup(int root, int bottom)
{
	int parent; //parent node (in the virtual tree) of the bottom element

	if (bottom > root)
	{
		parent = (bottom - 1) / D;

		//compare the two node and if the order is wrong then swap them
		//and make a recursive call to continue upward in the virtual tree
		//until the whole tree heap order is restored   
		if (*arr[parent] > *arr[bottom])
		{
			swap(arr[parent], arr[bottom]);
			reheapup(root, parent);
		}
	}
}


template<class T>
void Queue<T>::reheapdown(int root, int bottom)
{
	int minchild, firstchild, child;

	firstchild = root*D + 1; //the position of the first child of the root

	if (firstchild <= bottom) //if the child is in the queue
	{
		minchild = firstchild; //first child is the min child (temporarily)

		for (int i = 2; i <= D; ++i)
		{
			child = root*D + i; //position of the next child
			if (child <= bottom) //if the child is in the queue
			{
				//if the child is less than the current min child
				//then it will be the new min child
				if (*arr[child] < *arr[minchild])
				{
					minchild = child;
				}
			}
		}

		//if the min child found is less then the root(parent node)
		//then swap them and call reheapdown() recursively and
		//continue to fix the order in the virtual tree downwards
		if (*arr[root] > *arr[minchild])
		{
			swap(arr[root], arr[minchild]);
			reheapdown(minchild, bottom);
		}
	}
}

template<class T>
void Queue<T>::swap(T* &a, T* &b)
{
	T* c;
	c = a;
	a = b;
	b = c;
}

template<class T>
Queue<T>::~Queue(void)
{
	delete[] arr;
}


class Tree
{
private:
	class Node
	{
	public:
		unsigned int freq;
		unsigned char ch;
		Node *left, *right;
		
		Node(void)
			:freq(0), ch('\0'), left(NULL), right(NULL) {}
	};

	Node *root;


	Tree(const Tree &);
	const Tree & operator=(const Tree &);
	void chop(Node * N);
	void print(ostream &, Node *, int) const;
	void print(Node *, int) const;

public:
	Tree(void);
	~Tree(void);
	friend ostream & operator<<(ostream &, const Tree &);

	unsigned int get_freq(void) const;
	unsigned char get_char(void) const;
	void set_freq(unsigned int);
	void set_char(unsigned char);
	Node* get_left(void) const;
	Node* get_right(void) const;
	void set_left(Node *);
	void set_right(Node *);
	Node* get_root(void) const;

	
	bool operator==(const Tree &) const;
	bool operator!=(const Tree &) const;
	bool operator<(const Tree &) const;
	bool operator>(const Tree &) const;
	bool operator<=(const Tree &) const;
	bool operator>=(const Tree &) const;
	void huf(Node *, unsigned char, string, string &) const;
	void huf_list(Node *, string) const;
	bool get_huf_char(string, unsigned char &) const;
	string print_char(Node *) const;
};

Tree::Tree(void)
{
	Node* N = new Node;
	root = N;
}

void Tree::chop(Node *N)
{
	if (N)
	{
		chop(N->left);
		chop(N->right);
		delete N;
	}
}

Tree::~Tree(void)
{
	chop(root);
}

unsigned int Tree::get_freq(void) const
{
	return root->freq;
}

unsigned char Tree::get_char(void) const
{
	return root->ch;
}

void Tree::set_freq(unsigned int f)
{
	root->freq = f;
}

void Tree::set_char(unsigned char c)
{
	root->ch = c;
}

Tree::Node* Tree::get_left(void) const
{
	return root->left;
}

Tree::Node* Tree::get_right(void) const
{
	return root->right;
}

void Tree::set_left(Node* N)
{
	root->left = N;
}

void Tree::set_right(Node* N)
{
	root->right = N;
}

Tree::Node* Tree::get_root(void) const
{
	return root;
}

void Tree::print(ostream & ost, Node * curr, int level) const
{
}

void Tree::print(Node * curr, int level) const
{

}

ostream & operator<<(ostream &ost, const Tree &t)
{
	t.print(ost, t.root, 1);
	return ost;
}


bool Tree::operator==(const Tree & T) const
{
	return (root->freq == T.root->freq);
}

bool Tree::operator!=(const Tree & T) const
{
	return (root->freq != T.root->freq);
}

bool Tree::operator<(const Tree & T) const
{
	return (root->freq < T.root->freq);
}

bool Tree::operator>(const Tree & T) const
{
	return (root->freq > T.root->freq);
}

bool Tree::operator<=(const Tree & T) const
{
	return (root->freq <= T.root->freq);
}

bool Tree::operator>=(const Tree & T) const
{
	return (root->freq >= T.root->freq);
}


void Tree::huf(Node* N, unsigned char c, string str, string & s) const
{
	if (N)
	{
		
		if (!N->left && !N->right && N->ch == c)
		{
			s = str;
		}
		else
		{
			
			huf(N->left, c, str + "0", s);
			huf(N->right, c, str + "1", s);
		}
	}
}


void Tree::huf_list(Node* N, string str) const
{


	if (N)
	{
		if (!N->left && !N->right)
		{
			Table << print_char(N) << " " << str << endl;
		}
		else
		{
			huf_list(N->left, str + "0");
			huf_list(N->right, str + "1");
		}
	}
}


bool Tree::get_huf_char(string s, unsigned char & c) const
{

	for (int i = 0; i < 256; i++)
	{
		if (s == table[i])
		{
			c = (char)i;
			return true;
		}

	}
	return false;
}

string Tree::print_char(Node * N) const
{
	string s = "";

	if (!N->left && !N->right) //if it is a leaf node
	{
		unsigned char c = N->ch;

		//if the char is not printable then output its octal ASCII code
		if (c == 32) //32:blank char
		{
			s = '@';
		}
		else if (c == 10)
		{
			s = '>';
		}
		else if (c == 13)
		{
			s = '<';
		}
		else
			s = c;
	}
	return s;
}


void huf_write(unsigned char i, ofstream & outfile)
{
	static int bit_pos = 0; //0 to 7 (left to right) on the byte block
	static unsigned char c = '\0'; //byte block to write

	if (i<2) //if not EOF
	{
		if (i == 1)
			c = c | (i << (7 - bit_pos)); //add a 1 to the byte
		else //i==0
			c = c & static_cast<unsigned char>(255 - (1 << (7 - bit_pos))); //add a 0
		++bit_pos;
		bit_pos %= 8;
		if (bit_pos == 0)
		{
			outfile.put(c);
			c = '\0';
		}
	}
	else
	{
		outfile.put(c);
	}
}


unsigned char huf_read(ifstream & infile)
{
	static int bit_pos = 0; //0 to 7 (left to right) on the byte block
	static unsigned char c = infile.get();

	unsigned char i;

	i = (c >> (7 - bit_pos)) % 2; //get the bit from the byte
	++bit_pos;
	bit_pos %= 8;
	if (bit_pos == 0)
		if (!infile.eof())
		{
			c = infile.get();
		}
		else
			i = 2;

	return i;
}

void encoder()
{
	ifstream infile("Deneme.txt" , ios::in | ios::binary);
	if (!infile)
	{
		cout  << "Deneme.txt acilamadi!" << endl;
		system("pause");
		return;
	}

	ofstream outfile("Bin.dat", ios::out | ios::binary);
	
	if (!outfile)
	{
		cout << "Bin.dat acilamadi!" << endl;
		system("pause");
		return;
	}

	unsigned int f[256];
	for (int i = 0; i<256; ++i)
		f[i] = 0;

	char c;
	unsigned char ch;
	while (infile.get(c))
	{
		ch = c;
		++f[ch];
	}

	infile.clear();
	infile.seekg(0);

	Queue<Tree> q(3);
	Tree* tp;

	for (int i = 0; i<256; ++i)
	{


		outfile.put(static_cast<unsigned char>(f[i] >> 24));
		outfile.put(static_cast<unsigned char>((f[i] >> 16) % 256));
		outfile.put(static_cast<unsigned char>((f[i] >> 8) % 256));
		outfile.put(static_cast<unsigned char>(f[i] % 256));

		if (f[i]>0)
		{
			
			tp = new Tree;
			(*tp).set_freq(f[i]);
			(*tp).set_char(static_cast<unsigned char>(i));
			q.enq(tp);
		}
	}


	Tree* tp2;
	Tree* tp3;

	do
	{
		tp = q.deq();
		if (!q.empty())
		{

			tp2 = q.deq();
			tp3 = new Tree;
			(*tp3).set_freq((*tp).get_freq() + (*tp2).get_freq());
			(*tp3).set_left((*tp).get_root());
			(*tp3).set_right((*tp2).get_root());
			q.enq(tp3);
		}
	} while (!q.empty());

	string H_table[256];
	unsigned char uc;
	for (unsigned short us = 0; us<256; ++us)
	{
		H_table[us] = "";
		uc = static_cast<unsigned char>(us);
		(*tp).huf((*tp).get_root(), uc, "", H_table[us]);
	}

		Table.open("Tablo.txt", ios::out);
		(*tp).huf_list((*tp).get_root(), "");
		sayac = (*tp).get_freq();
		Table << "# " << sayac << endl;
		Table.close();

	unsigned char ch2;
	while (infile.get(c))
	{
		ch = c;
		for (unsigned int i = 0; i<H_table[ch].size(); ++i)
		{
			if (H_table[ch].at(i) == '0')
				ch2 = 0;
			if (H_table[ch].at(i) == '1')
				ch2 = 1;
			huf_write(ch2, outfile);
		}
	}
	ch2 = 2;
	huf_write(ch2, outfile);

	infile.close();
	outfile.close();

}


void decoder()
{
	string str;
	ifstream infile("Bin.dat" , ios::in | ios::binary);
	if (!infile)
	{
		cout << "Bin.dat acilamadi!" << endl;
		system("pause");
		return;
	}
	string tablo[256];
	ifstream tableFile("Tablo.txt", ios::in | ios::binary);
	if (!infile)
	{
		cout << "Tablo.txt acilamadi!" << endl;
		system("pause");
		return;
	}
	char inchar;
	string b;
	while (tableFile >> inchar >> b)
	{
		if (inchar == '@')
		{
			tablo[32] = b;
		}
		else if (inchar == '<')
		{
			tablo[13] = b;
		}
		else if (inchar == '>')
		{
			tablo[10] = b;
		}
		else if (inchar == '#')
		{
			sayac = atoi(b.c_str());
		}
		else
		{
			tablo[int(inchar)] = b;
		}

	}
	fill_n(table, 256," ");
	copy(tablo, tablo + 256, table);


	unsigned int f[256];
	char c;
	unsigned char ch;
	unsigned int j = 1;
	for (int i = 0; i<256; ++i)
	{
		f[i] = 0;
		for (int k = 3; k >= 0; --k)
		{
			infile.get(c);
			ch = c;
			f[i] += ch*(j << (8 * k));
		}
	}

	Queue<Tree> q(3);
	Tree* tp;



	for (int i = 0; i<256; ++i)
	{
		if (f[i]>0)
		{
			tp = new Tree;
			(*tp).set_freq(f[i]);
			(*tp).set_char(static_cast<unsigned char>(i));
			q.enq(tp);
		}
	}

	Tree* tp2;
	Tree* tp3;

	do
	{
		tp = q.deq();
		if (!q.empty())
		{

			tp2 = q.deq();
			tp3 = new Tree;
			(*tp3).set_freq((*tp).get_freq() + (*tp2).get_freq());
			(*tp3).set_left((*tp).get_root());
			(*tp3).set_right((*tp2).get_root());
			q.enq(tp3);
		}
	} while (!q.empty());

	if (1)
	{
		
		(*tp).huf_list((*tp).get_root(), "");
	}

	string st;
	unsigned char ch2;
	unsigned int total_chars = (*tp).get_freq();
	while (sayac>0)
	{
		st = "";
		do
		{
			ch = huf_read(infile);
			
			if (ch == 0)
				st = st + '0';
			if (ch == 1)
				st = st + '1';
		} //search the H. tree
		while (!(*tp).get_huf_char(st, ch2));

		str += ch2;
		--sayac;
	}

	infile.close();
	cout << str << endl << endl;
}

